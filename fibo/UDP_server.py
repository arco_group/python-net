#!/usr/bin/python -u
# Copyright: See AUTHORS and COPYING
"Usage: {0} <port>"

import sys, time
from socket import *

import jobs

class Handler:
    def __init__(self, sock, jobname='fibo'):
        self.sock = sock
        self.job = jobs.get(jobname)
        self.n = 0

    def handle(self, msg, client):
        self.n += 1
        print("New request {0} {1}".format(self.n, client))
        self.sock.sendto(self.job(msg), client)


def main(port):
    sock = socket(AF_INET, SOCK_DGRAM)
    sock.bind(('', port))
    handler = Handler(sock)

    while 1:
        msg, client = sock.recvfrom(1024)
        handler.handle(msg, client)


if len(sys.argv) != 2:
    print(__doc__.format(__file__))
    sys.exit(1)

try:
    main(int(sys.argv[1]))
except KeyboardInterrupt:
    sys.exit(0)
