#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

def fibo(n):
    try:
        n = int(n.strip('\n'))
    except ValueError:
        return '-1\n'

    a, b = 0, 1
    for i in range(n):
        a, b = b, a+b
    return str(a) + '\n'


def get(name):
    return globals()[name]
