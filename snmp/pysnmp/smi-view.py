#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

"With SMI convert between OID and text paths"

from pysnmp.smi import builder, view

mibBuilder = builder.MibBuilder().loadModules('SNMPv2-MIB')
mibViewController = view.MibViewController(mibBuilder)

oid, label, suffix = mibViewController.getNodeName(
    (1, 3, 6, 1, 2, 'mib-2', 1, 'sysDescr'))

print 'OID:', oid
print 'label:', label
print 'suffix:', suffix
