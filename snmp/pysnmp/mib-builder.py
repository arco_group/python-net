#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

"load and manage the MIB sysDescr variable"

from pysnmp.smi import builder

# create MIB builder
mibBuilder = builder.MibBuilder().loadModules('SNMPv2-MIB', 'IF-MIB')

# get Managed Object definition by symbol name
node, = mibBuilder.importSymbols('SNMPv2-MIB', 'sysDescr')
print 'name:', node.getName()
print 'status:', node.getStatus()
print 'syntax:', repr(node.getSyntax())
