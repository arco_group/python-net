#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

"query iface table over the localhost SNMP agent"

from pysnmp.entity.rfc3413.oneliner import cmdgen

community = cmdgen.CommunityData('test-agent', 'public')
transport = cmdgen.UdpTransportTarget(('localhost', 161))


def snmpget(oid):
    errIndication, errStatus, errIndex, varBinds = \
        cmdgen.CommandGenerator().getCmd(community, transport, oid)

    if errIndication:
        print errIndication
        return 1

    if errStatus:
        print '{0} at {1}\n'.format(
            errStatus.prettyPrint(),
            errIndex and varBinds[int(errIndex) - 1] or '?')
        return 1

    return varBinds[0]


ifTable = (1,3,6,1,2,1,2,2,1)
ifInOctets = 10
ifOutOctests = 16

for iface in range(1, 3):
    for var in [ifInOctets, ifOutOctests]:
        print snmpget(ifTable + (var, iface))
