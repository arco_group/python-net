from socket import *
import sys, re

# Puerto estandar
POP3_PORT = 110

# Terminadores de linea
CR = '\r'
LF = '\n'
CRLF = CR+LF

class POP3:
    """
    *******************************************************
    ****** RFC 1939 - Post Office Protocol - Version 3 *****
    *******************************************************
    ********** http://www.ietf.org/rfc/rfc1939.txt **********
    *******************************************************
    Autor: Jose Domingo Lopez Lopez [Jan '06]
    Codigo e ideas extraidas de:
    www.opensource.apple.com/darwinsource/10.2.8/python-3/python/Lib/poplib.py
                Author: David Ascher <david_ascher@brown.edu>
                [heavily stealing from nntplib.py]
                Updated: Piers Lauder <piers@cs.su.oz.au> [Jul '97]

    Esta clase implementa el protocolo POP3 con todos sus comandos basicos
    y opcionales exceptuando APOP.
    
    Comandos basicos:
                USER nombre ..................... user (nombre:string)
                PASS password .................. pass_ (password:string)
                STAT ................................... stat ()
                LIST [msg] .......................... list (msg:int = None)
                RETR msg ........................... retr (msg:int)
                DELE msg ........................... dele (msg:int)
                NOOP .................................. noop ()
                RSET ................................... rset ()
                QUIT ................................... quit ()
    Comandos opcionales:
                TOP msg n .......................... top (msg:int, n:int = 0)
                UIDL [msg] ......................... uidl (msg:int = None)
    """    
    
    # Cuando creamos un objeto de la clase POP, se creara un socket que
    # conectara con el servidor indicado en el puerto que se especifique.
    # Si no se especifica ningun puerto, sera el 110 por defecto.
    # Por ultimo, leemos el mensaje de bienvenida. 
    def __init__ (self, host, port = POP3_PORT, debug_level = 0):
        self.debugging = debug_level
        self.host = host
        self.port = port
        self.clientSocket = None
        self.file = None
        try:
            # Creo un socket TCP
            self.clientSocket = socket (AF_INET, SOCK_STREAM)
            # Conectamos con el host deseado al puerto deseado
            # (self.port = 110 si no se especifica)
            self.clientSocket.connect ((self.host, int (self.port)))
            # Trataremos el socket como si de un fichero se tratase
            self.file = self.clientSocket.makefile ('r')
            # Obtenemos el mensaje de bienvenida que nos manda el servidor
            self.welcome = self.getRespuesta ()
            if self.debugging == 1:
                sys.stdout.write (self.welcome + '\n')
        except error:
            # Si se produjo algun error cerramos el socket y el fichero
            self.clientSocket.close ()
            sys.stderr.write ('-ERR: No se pudo establecer la conexion.\n')
            sys.stderr.write ('Revise el archivo pymail.conf y compruebe ')
            sys.stderr.write ('que servidor POP3 y el puerto son correctos.\n')
            sys.exit (1)
    
    def setDebugLevel (self, level):
        self.debugging = level
        
    def getDebugLevel (self):
        return self.debugging
    
    # Lee una linea del socket (recordamos que lo tratamos como si de un
    # fichero se tratase. 
    def _leerLinea (self):
        linea = self.file.readline ()
        if not linea:
            sys.stderr.write ('-ERR: End Of File\n')
            sys.exit (1)
        # Calculamos los octetos que componen la linea en cuestion.
        octetos = len (linea)
        # Borramos los terminadores que hemos recibido.
        if linea[-2:] == CRLF:
            return linea[:-2], octetos
        if linea[0] == CR:
            return linea[1:-1], octetos
        # Se devuelven la linea sin terminadores y los octetos que la forman.
        return linea[:-1], octetos     
    
    # Lee una linea del socket y confirma si no ha sido erronea (+OK)
    def getRespuesta (self):
        respuesta, octetos = self._leerLinea ()
        # c = respuesta [:1]
        # if c != '+':
            # sys.stderr.write (repr (respuesta))
        return respuesta
    
    # Lee la repuesta de un comando multilinea, estos acaban con '.'
    def getRespuestaLarga (self):
        lista = []
        octetos = 0
        respuesta = self.getRespuesta()
        c = respuesta [:1]
        if c == '+':
            # Leemos del socket hasta que encontremos '.'
            linea, o = self._leerLinea ()
            while linea != '.':
                octetos = octetos + o
                lista.append (linea)
                linea, o = self._leerLinea ()
            # Devolvemos la respuesta corta, la larga y los octetos totales.
        return respuesta, lista, octetos
    
    # Envia un comando que tiene una respuesta de una sola linea.
    def _enviarComandoCorto (self, cmd):
        self.clientSocket.send ('%s%s' % (cmd, CRLF))
        respuesta = self.getRespuesta ()
        # Devuelve la respuesta del servidor: 1 linea.
        if self.debugging == 1:
            sys.stdout.write ('* %s *   %s\n' % (cmd, repr (respuesta)))
        return respuesta
    
    # Envia un comando que tiene una respuesta multilinea.
    def _enviarComandoLargo (self, cmd):
        self.clientSocket.send ('%s%s' % (cmd, CRLF))
        respuesta, lista, octetos = self.getRespuestaLarga ()
        if self.debugging == 1:
            sys.stdout.write ('* %s *   %s\n' % (cmd, repr (respuesta)))
        # Devuelve la respuesta corta y todas las lineas que le siguen.
        return respuesta, lista, octetos
    
    # De aqui en adelante todos los comandos del protocolo.
    
    # Envia el usuario, devuelve la respuesta del servidor.
    # Ejemplo de respuesta: +OK send your password
    def user (self, usuario):
        return self._enviarComandoCorto ('%s %s' % ('USER', usuario))
    
    # Envia la password en texto plano.
    # Despues de esto nos encontramos en "TRANSACTION STATE"
    def pass_ (self, password):
        return self._enviarComandoCorto ('%s %s' % ('PASS', password))
    
    # Devuelve el estado de la bandeja de entrada.
    # Consta de el numero de mensajes y el tamano total.
    def stat (self):
        respuesta = self._enviarComandoCorto ('STAT')
        datos = respuesta.split()
        # datos[0] == '+OK'
        numMensajes = datos [1]
        tamMensajes = datos[2]
        return (numMensajes, tamMensajes)
    
    # Si no se indica un mensaje, devuelve: numeroMensaje octetos
    # para cada uno de los mensajes.
    # Si se indica un mensaje, devuelve su numero y sus octetos.
    def list (self, msg = None):
        if msg == None:
            return self._enviarComandoLargo ('LIST')
        else:
            return self._enviarComandoCorto ('%s %s' % ('LIST', msg))

    # Devuelve el mensaje indicado por "msg"
    def retr (self, msg):
        return self._enviarComandoLargo ('%s %s' % ('RETR', msg))
    
    # Elimina el mensaje "msg"
    def dele (self, msg):
        return self._enviarComandoCorto ('%s %s' % ('DELE', msg))
    
    # Establece el idle = 0.
    # Un uso puede ser para que la sesion no de timeout.
    def noop (self):
        return self._enviarComandoCorto ('NOOP')

    # Si algun mensaje ha sido marcado para ser borrado, lo desmarca.
    def rset (self):
        return self._enviarComandoCorto ('RSET')
    
    # Finaliza la sesion.
    # Se realizan los cambios en la bandeja de entrada.
    def quit (self):
        respuesta = self._enviarComandoCorto ('QUIT')
        self.file.close()
        self.clientSocket.close()
        return respuesta

    # Devuelve la cabecera del mensaje "msg" con las "n" primeras
    # lineas del cuerpo del mensaje.
    def top (self, msg, n = 0):
        return self._enviarComandoLargo ('%s %s %s' % ('TOP', msg, n))
    
    # Devuelve un identificador unico que posee el mensaje.
    # Si no se especifica un mensaje devuelve todos los mensajes con sus
    # especificadores unicos.
    # Este UID es generado por el servidor.
    def uidl (self, msg = None):
        if msg == None:
            return self._enviarComandoLargo ('UIDL')
        else:
            return self._enviarComandoCorto ('%s %s' % ('UIDL', msg))
