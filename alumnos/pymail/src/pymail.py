#!/usr/bin/python

import pop, smtp, string, sys
from configure import *

CR = '\r'
LF = '\n'
CRLF = CR+LF

incoming = None
configuracion = None
debug_level = 0

def showInfo (mensaje):
    date = ''
    from_ = ''
    subject = ''
    # Recuperamos la cabecera del mensaje indicado
    respuesta, header, octetos = incoming.top (mensaje)
    for j in header:
        field = j.split ('\n')
        for i in field:
            if i[:4] == 'Date': date = i[5:]
            if i[:4] == 'From': from_ = i[5:]
            if i [:7] == 'Subject': subject = i[8:]
    sys.stdout.write ('%s %s  %s   %s\n' %  (mensaje, from_, date, subject))

def bandejaEntrada (msg = 0):
    # Si no definimos un mensaje
    # mostramos la info de toda la bandeja de entrada
    if msg == 0:
        resp_list, lista_list, octetos_list = incoming.list ()
        for i in lista_list:
            # i contiene el numero del mensaje, un espacio y los octetos
            mensaje = i.split (' ')
            # solo nos interesa el numero del mensaje
            showInfo (mensaje[0])
    # si definimos un mensaje
    # mostramos info relativa a dicho mensaje
    else:
        resp = incoming.list (msg)
        # resp = "+OK <numero> <octetos>
        mensaje = resp.split (' ')
        showInfo (mensaje[1])
    
def leerEMail (objetivo):
        email = ""
        headers = ""
        # Obtenemos el mensaje indicado
        respuesta, mensaje, octetos = incoming.retr (objetivo)
        if octetos != 0:
            condicion = raw_input ('Deseas ver el mensaje con cabeceras? (s/n): ')
            while condicion != 's' and condicion != 'n':
                sys.stdout.write ('Opcion incorrecta, teclea \'s\' o \'n\'.')
                condicion = raw_input ('Deseas ver el mensaje con cabeceras? (s/n): ')
            # Anadimos salto de linea al final de cada linea que hemos recibido
            for i in mensaje:
                email = email + i + '\n'
            # Si NO queremos mostrar las cabeceras
            if condicion == 'n':
                # obtenemos la cabecera y se la cortamos al mensaje
                respuesta, cabecera, octetos = incoming.top(objetivo)
                for j in cabecera:
                    headers = headers + j + '\n'
                email = email [(len(headers)-1):]
        sys.stdout.write ('\n' + email)

def enviarEMail (destino, email):
    # Creamos una instancia de SMTP para correo saliente pasandole  
    # el servidor y el puerto que previamente hemos leido de la configuracion
    outgoing = smtp.SMTP (configuracion.getSMTPServer (), configuracion.getSMTPPort (), debug_level)
    # "Saludamos" al sevidor
    outgoing.helo()
    # Indicamos quien envia el e-mail, por defecto la cuenta configurada
    outgoing.mail (configuracion.getUser ())
    # Enviamos el email a cada uno de los destinos
    # ejemplo: usuario@dominio.es,user@domain.com
    recipe = destino.split (',')
    for i in recipe:
        outgoing.rcpt (i)
    # Enviamos el e-mail previamente construido
    outgoing.data (email)
    # Logout
    outgoing.quit ()
    
def escribirEMail ():
    data = ''
    destino = raw_input ('Destino: ')
    while 1:
        otro_destino = raw_input ('Deseas anadir mas destinos? (s/n): ')
        if otro_destino == 's':
            destino = destino + ',' + raw_input ('Destino: ')
        if otro_destino == 'n': break
        if otro_destino != 's' and otro_destino != 'n':
            sys.stdout.write ('Opcion incorrecta. Reintentelo.\n')
    asunto = raw_input ('Asunto: ')
    sys.stdout.write ('Escribe el mensaje, para terminar pulsa Ctrl+D.\n')
    while 1:
        line = sys.stdin.readline ()
        if not line: break
        data = data + line
    new_mail = 'From:' + configuracion.getUser () + CRLF
    new_mail = new_mail + 'To:' + destino + CRLF
    new_mail = new_mail + 'Subject:' + asunto + CRLF + CRLF
    new_mail = new_mail + data + CRLF + '.' + CRLF
    enviarEMail (destino, new_mail)
    
def menu ():
    global incoming
    global debug_level
    
    while 1:
        opcion = raw_input ('& ')
        if opcion.isdigit ():
            leerEMail (opcion)
        if opcion[:1] == 'd' and opcion[2:].isdigit ():
            incoming.dele (opcion[2:])
        if opcion[:1] == 'p':
            if opcion [2:].isdigit (): bandejaEntrada (opcion[2:])
            else: bandejaEntrada ()
        if opcion[:1] == 'w':
            escribirEMail ()
        if opcion[:1] == 'r': 
            incoming.rset ()
        if opcion == 'q':
            break
        if opcion[:1] == '?':
            help = open ('pymail.help', 'r')
            linea = help.readline ()
            while 1:
                if not linea: break
                sys.stdout.write (linea)
                linea = help.readline ()
            help.close ()
            sys.stdout.write ('\n')
        if opcion[:1] == 'c':
            if debug_level == 1:
                debug_level = 0
                incoming.setDebugLevel (0)
            else:
                sys.stderr.write ('*Err: debug mode already deactivated\n')
        if opcion[:1] == 's':
            if debug_level == 0:
                debug_level = 1
                incoming.setDebugLevel (1)
            else:
                sys.stderr.write ('*Err: debug mode already activated\n')

def main ():
    global debug_level
    global incoming
    global configuracion
    
    sys.stdout.write ('pyMail version 0.1: Yet Another E-Mail Client.\n')
    sys.stdout.write ('\nEste programa es software libre y es distribuido\n')
    sys.stdout.write ('con el fin de ser util pero SIN GARANTIA ALGUNA.\n')
    sys.stdout.write ('Para mas informacion acerca lea el apartado LICENCIA\n')
    sys.stdout.write ('de la documentacion suministrada con el programa.\n')
    sys.stdout.write ('\nEscribe ? para obtener ayuda.\n')
    while 1:
        debug_level = raw_input ('Deseas mostrar la informacion del protocolo? (s/n): ')
        if debug_level == 's' or debug_level == 'n': break
    if debug_level == 's': debug_level = 1
    else: debug_level = 0
    # Creamos una instancia de Configure para escribir/leer la configuracion
    # le pasamos el archivo de configuracion y los modos de acceso
    configuracion = Configure ('./pymail.conf','r+')
    # Creamos una instancia de POP3 para el correo entrante
    incoming = pop.POP3 (configuracion.getPOP3Server (), configuracion.getPOP3Port (), debug_level)
    # Si no se ha producido ningun error en la conexion seguimos adelante
    sys.stdout.write ('Conectado con %s:%s\n' % (configuracion.getPOP3Server (), configuracion.getPOP3Port ()))
    # Nos indentificamos con el servidor
    ulogin = incoming.user (configuracion.getUser ())
    plogin = incoming.pass_ (configuracion.getPassword ())
    if ulogin[:3] == '+OK' and plogin[:3] == '+OK':
        sys.stdout.write ('Login correcto.\n')
    else:
        sys.stderr.write ('Usuario/password erroneo.\n')
        sys.stderr.write ('Edita el archivo pymail.conf con la informacion correcta.\n')
        sys.exit (1)
    numMensajes, tamMensajes = incoming.stat ()
    sys.stdout.write ('Tienes %s mensajes en la bandeja de entrada.\n' % numMensajes)
    
    while 1:
        check = raw_input ('Deseas ver la bandeja de entrada ahora? (s/n): ')
        if check == 's' or check == 'n': break
    if check == 's': bandejaEntrada ()
    
    menu ()
    incoming.quit ()

if __name__ == "__main__":
    try:
        main ()
    except KeyboardInterrupt:
        if incoming != None:
            incoming.quit ()
