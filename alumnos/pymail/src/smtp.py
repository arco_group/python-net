from socket import *
import sys

# Puerto estandar
SMTP_PORT = 25

# Terminadores de linea
CR = '\r'
LF = '\n'
CRLF = CR+LF

class SMTP:
    """
    *******************************************************
    ****** RFC 2821 - Simple Mail Transfer Protocol ********
    *******************************************************
    ********** http://www.ietf.org/rfc/rfc2821.txt **********
    *******************************************************
    Autor: Jose Domingo Lopez Lopez
    [heavily stealing from nntplib.py]
    
    Esta clase implementa el protocolo SMTP con los comandos basicos
    que se utilizan para enviar un correo electronico.
    
    Comandos :
                HELO .................................................. helo ()
                EHLO .................................................. ehlo ()
                HEPL ................................................... help ()
                MAIL FROM: .................... mail (source:string)
                RCPT TO: ........................ rcpt (destiny:string)
                DATA ................................. data (body:string)
                QUIT ..................................................... quit ()
    """
    
    def __init__ (self, host, port = SMTP_PORT, debug_level = 0):
        self.debugging = debug_level
        self.host = host
        self.port = port
        self.clientSocket = None
        self.file = None
        try:
            # Creo un socket TCP
            self.clientSocket = socket (AF_INET, SOCK_STREAM)
            # Conectamos con el host deseado al puerto deseado
            # (self.port = 25 si no se especifica)
            self.clientSocket.connect ((self.host, int (self.port)))
            # Trataremos el socket como si de un fichero se tratase
            self.file = self.clientSocket.makefile ('r')
             # Obtenemos el mensaje de bienvenida que nos manda el servidor
            self.welcome = self.getRespuesta ()
            if self.debugging == 1:
                sys.stdout.write (self.welcome + '\n')
        except error:
            # Si se produjo algun error cerramos el socket y el fichero
            self.clientSocket.close ()
            sys.stderr.write ('-ERR: No se pudo establecer la conexion.\n')
            sys.stderr.write ('Revise el archivo pymail.conf y compruebe ')
            sys.stderr.write ('que servidor SMTP y el puerto son correctos.\n')
            sys.exit (1)

    def setDebugLevel (self, level):
        self.debugging = level
        
    def getDebugLevel (self):
        return self.debugging

    def _leerLinea (self):
        linea = self.file.readline ()
        if not linea:
            sys.stderr.write ('-ERR: End Of File\n')
            sys.exit (1)
        if linea[-2:] == CRLF:
            return linea[:-2]
        if linea[0] == CR:
            return linea[1:-1]
        return linea   

    def getRespuesta (self):
        respuesta = self._leerLinea ()
        # c = respuesta [:1]
        # Las operaciones correctas se contestan con el codigo 2XX
        # if c != '2' and c!= '3':
            # sys.stderr.write (respuesta)
        return respuesta

    def getRespuestaLarga (self):
        respuesta = self.getRespuesta ()
        resp = []
        while 1:
            linea = self.file.readline()
            resp.append (linea)
            # Cuando hay una respuesta multilinea, todas las lineas excepto
            # la ultima tienen el formato: XXX-Mensaje
            # donde XXX es el codigo, - indica que hay mas lineas
            # La ultima linea tiene el formato: XXX Mensaje
            if linea[3:4] != '-': break
        return respuesta, resp
    
    # Envia un comando que tiene una respuesta de una sola linea.
    def _enviarComandoCorto (self, cmd):
        self.clientSocket.send ('%s%s' % (cmd, CRLF))
        respuesta = self.getRespuesta ()
        # Devuelve la respuesta del servidor: 1 linea.
        if self.debugging == 1:
            sys.stdout.write ('* %s *   %s\n' % (cmd, repr (respuesta)))
        return respuesta
    
    # Envia un comando que tiene una respuesta multilinea.
    def _enviarComandoLargo (self, cmd):
        self.clientSocket.send ('%s%s' % (cmd, CRLF))
        respuesta, respuesta2 = self.getRespuestaLarga ()
        if self.debugging == 1:
            sys.stdout.write ('* %s *   %s\n          %s\n' % (cmd, repr (respuesta), repr (respuesta2)))
        # Devuelve la respuesta corta y todas las lineas que le siguen.
        return respuesta, respuesta2
    
    def helo (self):
        return self._enviarComandoCorto ('HELO')
        
    def ehlo (self):
        return self._enviarComandoLargo ('EHLO')
        
    def help (self):
        return self._enviarComandoCorto ('HELP')
        
    def mail (self, mail):
        return self._enviarComandoCorto ('MAIL FROM:%s' % mail)
        
    def rcpt (self, mail):
        return self._enviarComandoCorto ('RCPT TO:%s' % mail)
    
    def data (self, data):
        return self._enviarComandoCorto ('DATA' + CRLF + data)
    
    def quit (self):
        respuesta = self._enviarComandoCorto ('QUIT')
        self.file.close()
        self.clientSocket.close()
        return respuesta
