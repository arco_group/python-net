import string, sys
from getpass import getpass
from configure import *

class Configure:
    def __init__ (self, archivoConfiguracion, modos):
        self.pop3_server = None
        self.pop3_port = None
        self.user = None
        self.password = None
        self.smtp_server = None
        self.smtp_port = None
        try:
            # Abrimos el archivo indicado con los modos deseados
            self.fichero = open (archivoConfiguracion, modos)
            # Leemos la primera linea del fichero y...
            self.linea = self.fichero.readline ()
            # ... si no hay nada, lanzamos el asistente de configuracion
            if not self.linea: self.asistenteConfiguracion ()
            # ... si leemos algo, leemso la configuracion para ejecutar
            # el programa con normalidad
            else: self.leerConfiguracion ()
            # Una vez finalizada la operacion, cerramos el fichero
            self.fichero.close()
        except IOError:
            sys.stderr.write ('Error abriendo el fichero %s\n' % archivoConfiguracion)
            sys.exit (1)
        
    def getPOP3Server (self):
        return self.pop3_server
        
    def getPOP3Port (self):
        return self.pop3_port
        
    def getUser (self):
        return self.user
        
    def getPassword (self):
        return self.password
        
    def getSMTPServer (self):
        return self.smtp_server
        
    def getSMTPPort (self):
        return self.smtp_port
    
    def _conf (self):
        opciones = None
        valor = ''
        # Nunca reposicionamos el puntero para ir avanzando a lo largo
        # del archivo de configuracon
        # Como las opciones son de tipo: opcion=valor ...
        opciones = self.linea.split ('=')
        valor = opciones [1]
        # Y leemos la siguiente linea
        self.linea = self.fichero.readline ()
        # Retornamos el valor quitando el ultimo caracter que es fin de linea
        # y el puntero a la linea que contiene la siguiente opcion
        return valor[:len(valor)-1], self.linea

    def leerConfiguracion (self):
        self.pop3_server, self.linea = self._conf ()
        self.pop3_port, self.linea = self._conf ()
        self.user, self.linea = self._conf ()
        self.password, self.linea = self._conf ()
        self.smtp_server, self.linea = self._conf ()
        self.smtp_port, self.linea = self._conf ()

    def asistenteConfiguracion (self):
        mensajeControl = ''
        lista = []
        if not self.linea:
            sys.stdout.write ('pyMail no esta configurado en este momento.\n')
            sys.stdout.write ('Siga las instrucciones del asistente, por favor.\n')
        
            self.pop3_server = raw_input ("Servidor POP3 (correo entrante): ")
            lista.append ('pop3_server=%s\n' % self.pop3_server)
            
            self.pop3_port = raw_input ("Puerto POP3 (default 110): ")
            if self.pop3_port == '': self.pop3_port = '110'
            lista.append ('pop3_port=%s\n' % self.pop3_port)
            
            self.user = raw_input ("Usuario: ")
            lista.append ('user=%s\n' % self.user)
            
            self.password = getpass ("Password (sin eco): ")
            lista.append ('password=%s\n' % self.password)
            
            self.smtp_server = raw_input ("Servidor SMTP (correo saliente): ")
            lista.append ('smtp_server=%s\n' % self.smtp_server)
            
            self.smtp_port = raw_input ("Puerto SMTP (default 25): ")
            if self.smtp_port == '': self.smtp_port = '25'
            lista.append ('smtp_port=%s\n' % self.smtp_port)
            
            self.fichero.writelines (lista)
            mensajeControl = 'Se ha terminado la configuracion.\n'
        else:
            mensajeControl = 'El archivo de configuracion debe estar vacio.\n'
        sys.stdout.write (mensajeControl)
    
    
