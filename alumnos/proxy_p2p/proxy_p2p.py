#!/usr/bin/python

#############################################################################
## UCLM. Redes(Sistemas). Curso 2005-2006. Alumno: Javier Ibanez Lumbreras ##
#############################################################################

from sys import argv, exit
import random, os, thread, socket, time, threading
from socket import AF_INET, SOCK_STREAM
from select import select

# se actualizaran las listas en un tiempo aleatorio dentro de este rango de tiempo (en segundos)
retardo = (60,100)

# cantidad de nodos que se escogeran para actualizar las listas
selec_nodos = 10

# numero maximo de conexiones activas con los proxys
MAX_CHILDREN = 40;

# lista de hijos activos
children = [];

# mensaje de ayuda
help="""\nUSO:
proxy_p2p.py <Web_port> <p2p_port> <listas> [-pr <proxy>] [-fr <friend>]
\nOPCIONES:
<Web_port>: puerto por el cual se atendera a los clientes Web
<p2p_port>: puerto local para las comunicaciones con la red de nodos proxy_p2p
<listas>: nombre base para las listas de proxys y nodos
[-pr <proxy>]: direccion:puerto de un proxy para contribuir a lista de proxys
[-fr <friend>]: direccion:puerto de un nodo de una red proxy_p2p
""" 

# Excepciones
class parse_exception:
	pass

class timeout_exception:
	pass

class lista_vacia_exception:
	pass

# leera de un archivo una lista de direcciones y puertos
def leerlista(nombre):
	lista = []
	try:
		origen = open(nombre,"r")
	except IOError:	# si no se puede abrir se devolvera una lista vacia
		return lista
	for linea in origen:
		try:
			lista.append(str2addr(linea[:-1]))
		except parse_exception: # no se pudo convertir en la tupla (dir,puerto)
			continue
	origen.close()
	return lista

# escribe el contenido de la lista en un archivo
def escribirlista(nombre, lista):
	try:
		destino = open(nombre,"w")
	except IOError:
		raise
	for elemento in lista:
		destino.write(elemento[0]+':'+str(elemento[1])+'\n')
	destino.flush()
	destino.close()

# convierte el formato "dir:puerto" en una tupla (dir, puerto) siendo "dir" una direccion IP y "puerto" un entero
def str2addr(linea):
	try:
		return (socket.gethostbyname(linea.split(":")[0]),int(linea.split(":")[1]))
	except (ValueError, IndexError, socket.gaierror): # si falla la conversion se lanza la excepcion str_to_addr_exception
		raise parse_exception

# lee una linea del socket. Se puede indicar un timeout (en segundos)
def leerlinea_socket(sock, timeout=None):
	if timeout:
		rd = select([sock],[],[],timeout)[0]
		if not rd:
			raise timeout_exception
	linea=""
	while 1:
		a = sock.recv(1)
		if not a or a=='\n': break
		linea+=a
	return linea

# se finaliza el programa y se deben guardar la lista de proxys y nodos en archivos
def salirOrdenadamente():
	try:
		escribirlista(nombre_listas+"_proxys.lst", lista_proxys)
		escribirlista(nombre_listas+"_nodos.lst", lista_nodos)
	except IOError:
		raise

def collect_children(children):
	while children:
		if len(children) < MAX_CHILDREN:
			opts = os.WNOHANG
		else:
			opts = 0;
		pid, status = os.waitpid(0,opts)
		if not pid: break
		children.remove(pid)

# actualiza las listas pidiendo a todos los nodos las suyas
def actualizar():

	# Se seleccionaran "selec_nodos" numero de nodos para evitar exceso de trafico en la red
	lista_nodos_seleccionados = []
	for nodo in lista_nodos: # se copia la lista de nodos a la lista de nodos seleccionados
		lista_nodos_seleccionados.append(nodo)
	
	while len(lista_nodos_seleccionados) > selec_nodos: # Se eliminan nodos hasta llegar a "selec_nodos" nodos
		lista_nodos_seleccionados.remove(random.choice(lista_nodos_seleccionados))
		
	# se pediran las listas de los nodos seleccionados
	for nodo in lista_nodos_seleccionados:
		try:
			# Se piden las listas de nodos
			amigo_sock = socket.socket(AF_INET,SOCK_STREAM)
			amigo_sock.connect(nodo)
			amigo_sock.sendall("getNodos\n")
			while 1: # se recibe la direccion y el puerto de los nodos y se guardan
				try:
					linea = leerlinea_socket(amigo_sock,10)
					if not linea:
						break
					nuevo_nodo = str2addr(linea)
					if nuevo_nodo not in lista_nodos:
						lock.acquire()
						lista_nodos.append(nuevo_nodo)
						lock.release()
				except parse_exception: # No se pudo convertir la linea en (dir,puerto)
					continue
			amigo_sock.close()

			# Se piden las listas de proxys
			amigo_sock = socket.socket(AF_INET,SOCK_STREAM)
			amigo_sock.connect(nodo)
			amigo_sock.sendall("getProxys\n")
			while 1: # se recibe la direccion y el puerto de los proxys y se guardan
				try:
					linea = leerlinea_socket(amigo_sock,10)
					if not linea: 
						break
					nuevo_proxy = str2addr(linea)
					if nuevo_proxy not in lista_proxys:
						# se comprueba que el proxy este vivo
						# se evita que proxys muertos circulen por la red indefinidamente
						nuevo_proxy_sock = socket.socket(AF_INET, SOCK_STREAM)
						try:
							nuevo_proxy_sock.settimeout(5) # timeout para el socket de 5 segundos
							nuevo_proxy_sock.connect(nuevo_proxy)
							nuevo_proxy_sock.close()
							# el proxy esta vivo
							lock.acquire()
							lista_proxys.append(nuevo_proxy)
							lock.release()
						except socket.error: # no se pudo conectar
							nuevo_proxy_sock.close()
				except parse_exception: # No se pudo convertir la linea en (dir,puerto)
					continue
			amigo_sock.close()
		except (timeout_exception, socket.error): # El nodo no responde o no se pudo conectar
			lista_nodos.remove(nodo)
			amigo_sock.close()

	# Se recorren los nodos para ver si estan vivos
	for nodo in lista_nodos: 
		amigo_sock = socket.socket(AF_INET, SOCK_STREAM)
		try:
			# se indica al nodo que estamos en la red p2p y nuestro p2p_port
			amigo_sock.settimeout(5) # timeout para el socket de 5 segundos
			amigo_sock.connect(nodo)
			amigo_sock.sendall("HELLO:"+str(p2p_port)+"\n")
			amigo_sock.close()
			
		except socket.error: # no se pudo conectar
			lista_nodos.remove(nodo)
			amigo_sock.close()

	print "Listas actualizadas"
	print "Proxys:"
	for proxy in lista_proxys:
		print proxy
	print "Nodos:"
	for nodo in lista_nodos:
		print nodo

# se encarga de comunicar el cliente web con un proxy elegido aleatoriamente
def redirigir(cliente_sock):
	while 1:
		try:
			proxy = random.choice(lista_proxys) # se elige aleatoriamente un proxy
			proxy_sock = socket.socket(AF_INET,SOCK_STREAM)
			proxy_sock.settimeout(5) # timeout para el socket de 5 segundos
			print "Se intenta conectar con el proxy:",proxy
			proxy_sock.connect(proxy)
			print "Se hizo una conexion con el proxy:",proxy
			break
		except (socket.error, socket.gaierror): # si no responde se elimina de la lista y se elige otro proxy
			lock.acquire()
			print "El proxy",proxy,"no responde, se elimina."
			lista_proxys.remove(proxy)
			lock.release()
			proxy_sock.close()
		except IndexError: # esto significa que la lista esta vacia
			raise lista_vacia_exception

	# se crea un proceso hijo que se encargara de comunicar el cliente con el proxy
	collect_children(children)
	pid = os.fork()

	if pid: # proceso padre
		children.append(pid) 
		return

	# proceso hijo: maneja la conexion
	try:
		while 1:
			rd = select([cliente_sock,proxy_sock],[],[])[0]
			if cliente_sock in rd:
				data=cliente_sock.recv(1024)
				if not data: 
					print "El cliente se desconecto"
					break
				proxy_sock.sendall(data)
			if proxy_sock in rd: 
				data=proxy_sock.recv(1024)
				if not data: 
					print "El proxy se desconecto"
					break
				cliente_sock.sendall(data)
	except socket.error:
		print "Error en la conexion. Se da por terminda"
		
	cliente_sock.shutdown(socket.SHUT_RDWR)
	proxy_sock.shutdown(socket.SHUT_RDWR)
	cliente_sock.close()
	proxy_sock.close()
	exit(0)

# Se encarga de responder a una peticion de otro nodo amigo
def atender_amigo(amigo_sock, amigo):
	try:
		linea = leerlinea_socket(amigo_sock) # se lee la peticion
		if not linea: # se ha desconectado
			return
		# Se procesa la peticion
		if linea.startswith("HELLO:"): # Nos indica que el nodo sigue vivo y su p2p_port
			# una peticion de este tipo seria: "HELLO:4060". 4060 seria su puerto p2p_port
			try:
				nodo = (amigo[0],int(linea[6:]))
				if nodo not in lista_nodos: # no esta en nuestra lista
					lock.acquire()
					lista_nodos.append(nodo)
					lock.release()
			except ValueError: # No indico correctamente el puerto
				return
		elif linea=="getProxys": # Nos pide nuestra lista de proxys
			for proxy in lista_proxys:
				amigo_sock.send(proxy[0]+':'+str(proxy[1])+'\n')
			amigo_sock.makefile().flush()
		elif linea=="getNodos": # Nos pide nuestra lista de nodos
			for nodo in lista_nodos:
				amigo_sock.send(nodo[0]+':'+str(nodo[1])+'\n')
			amigo_sock.makefile().flush()
	except timeout_exception: # se produjo un timeout leyendo el socket
		return

#############
## INICIO  ##
#############

# se comprueba que el numero de argumentos sea correcto
if not len(argv) in [4,6,8]:
	print help
	exit(0)

lista_proxys = []
lista_nodos = []

lock = threading.Lock()

# los argumentos se copian en variables
try:
	nombre_listas = argv[3]
	
	# Se copian las listas que pueda haber en los archivos
	lista_proxys = leerlista(nombre_listas+"_proxys.lst")
	lista_nodos = leerlista(nombre_listas+"_nodos.lst")

	web_port = int(argv[1])
	p2p_port = int(argv[2])
	if len(argv)==6:
		if argv[4]=="-pr":
			proxy = str2addr(argv[5])
			if not proxy in lista_proxys:
				lock.acquire()
				lista_proxys.append(proxy)
				lock.release()
		elif argv[4]=="-fr":
			nodo = str2addr(argv[5])
			if not nodo in lista_nodos:
				lock.acquire()
				lista_nodos.append(nodo)
				lock.release()
		else:
			print help
			exit(0)

	if len(argv)==8:
		if argv[4]!="-pr" or argv[6]!="-fr":
			print help
			exit(0)
		proxy = str2addr(argv[5])
		if not proxy in lista_proxys:
			lock.acquire()
			lista_proxys.append(proxy)
			lock.release()
		nodo = str2addr(argv[7])
		if not nodo in lista_nodos:
			lock.acquire()
			lista_nodos.append(nodo)
			lock.release()
		
except parse_exception: # Las direcciones estan mal formadas
	print "Las direcciones host:port no son correctas"
	print help
	exit(0)
except ValueError: # Los puerto no son correctos
	print "Los puertos no son correctos"
	print help
	exit(0)

# se inician los sockets
web_msock = socket.socket(AF_INET, SOCK_STREAM)
web_msock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
web_msock.bind(('', web_port))
web_msock.listen(5)

p2p_msock = socket.socket(AF_INET, SOCK_STREAM)
p2p_msock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
p2p_msock.bind(('', p2p_port))
p2p_msock.listen(5)

# al inicio se pone la alarma de forma que se actualicen las listas inmediatamente
alarma=time.time()

try:
	while 1:
		rd = select([web_msock,p2p_msock],[],[],1)[0]

		if time.time()>=alarma:
			print "Se inicia una actualizacion de listas"
			thread.start_new_thread(actualizar,())
			# la proxima actualizacion se realizara dentro de un valor aleatorio en el rango retardo
			alarma=time.time()+random.choice(range(retardo[0],retardo[1]))
			continue
		if web_msock in rd: # un cliente quiere comunicarse con un proxy
			cliente_sock, cliente = web_msock.accept()
			try:
				print "El cliente ",cliente," se ha conectado al proxy"
				redirigir(cliente_sock)
			except lista_vacia_exception: # La lista de proxys se quedo vacia...
				print "La lista de proxys se quedo vacia"
				cliente_sock.close()
			continue
		if p2p_msock in rd: # un nodo amigo nos quiere hacer una peticion
			amigo_sock, amigo_addr = p2p_msock.accept()
			print "El nodo",amigo_addr,"nos hizo una peticion"
			atender_amigo(amigo_sock, amigo_addr)
			print "Se termino de atender al nodo"
			amigo_sock.close()
except KeyboardInterrupt:
	print "Se produjo una interrupcion de teclado"
try:
	print "Se guardaran las listas en los archivos"
	salirOrdenadamente()
except IOError:
	print "No se pudo guardar las listas en los archivos correctamente"

