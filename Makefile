
wiki:
	hg clone ssh://hg@bitbucket.org/arco_group/python-net/wiki

clean:
	@find . -name "*~" -print | xargs rm -f
