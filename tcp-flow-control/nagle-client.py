#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-
"""\
Trivial TCP client to test nagle algorithm.
  - Use netcat as server: ncat -listen 2000 --keep-open
  - Capture traffic with: tshark -i lo -f "tcp port 2000"
  - Run this client:
    - enabling nagle: {0} localhost 2000 true
    - disabling nagle: {0} localhost 2000 false
  - Note the amount and len of segments

usage: {0} <host> <port> [true|false]
"""

import sys
import socket


if len(sys.argv) != 4:
    print(__doc__.format(__file__))
    sys.exit()

host = sys.argv[1]
port = int(sys.argv[2])
nagle = sys.argv[3] == 'true'

sock = socket.socket()

if not nagle:
    sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True)
    print('nagle is disabled')

sock.connect((host, port))

for i in range(1000):
    sock.send(b'a')

sock.close()
