#! /usr/bin/python
'''
  server: mcast.py
  client: mcast.py <msg> 
'''

import sys, socket, struct

if len(sys.argv) == 1:
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('', 6543))
    mreq = struct.pack('4sl',
                       socket.inet_aton('225.100.100.100'),
                       socket.INADDR_ANY)

    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

    while True:
        print sock.recv(1024)

else:
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
    sock.sendto(sys.argv[1], ('225.100.100.100', 6543))
