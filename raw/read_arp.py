import socket

ETH_P_ARP = 0x0806

s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(ETH_P_ARP))
s.bind('eth0')

while 1:
    print '--\n', repr(s.recv(1600))

