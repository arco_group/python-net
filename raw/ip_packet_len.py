#!/usr/bin/python
"Print IP packet len"

__author__ = "David Villa Alises"

import sys, os
import socket
import struct
import time
import signal


class IP_package:
    protos = {1: 'ICMP', 2: 'IGMP', 6: 'TCP', 17: 'UDP', 89: 'OSPF', 103: 'PIM'}

    def __init__(self, data):
        self.data = data
        self.len = struct.unpack('!H', self.data[2:4])[0]

    def get_proto(self):
        code = ord((self.data[9]))
        try:
            return self.protos[code]
        except KeyError:
            return code


class IP_Sniffer:
    def __init__(self):
        ETH_P_IP = 0x800
        self.sock = socket.socket(socket.AF_PACKET, socket.SOCK_RAW,
                                  socket.htons(ETH_P_IP))
        self.num_pkt = 0
        self.num_bytes = 0

    def capture(self):
        self.tini = time.time()
        while 1:
            try:
                self.capture_packet()
            except socket.error:
                break

        self.diff = time.time() - self.tini

    def capture_packet(self):
        pkt = IP_package(self.sock.recv(1600)[14:])

        self.num_pkt += 1
        self.num_bytes += pkt.get_len()

        print "proto: %s, len: %s" % (pkt.get_proto(), pkt.get_len())

    def print_stats(self):
        print
        print("Capture time: {:.2f}s".format(self.diff))
        print("{} pkgs, {:.2f} pkt/s".format(
                self.num_pkt, self.num_pkt / self.diff))
        print("{} bytes, {:.2f} bytes/s".format(
                self.num_bytes, self.num_bytes / self.diff))


def signal_handler(*args):
    pass


signal.signal(signal.SIGINT, signal_handler)
sniffer = IP_Sniffer()
sniffer.capture()
sniffer.print_stats()
