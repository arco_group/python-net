#!/usr/bin/python
"sniff and print hex dump of all ethernet frames"

import time, socket
from string import printable

# substitute non-printable chars to dots
CHARMAP = str.join('', [c if c in printable[:-5] else '.'
                        for c in map(chr, range(256))])

def hexdump(frame):
    def to_chr(bytes):
        retval = bytes.translate(CHARMAP)
        return retval[:8] + ' ' + retval[8:]

    def to_hex(bytes):
        retval = str.join(' ', ["%02X" % ord(x) for x in bytes])
        return retval[:23] + ' ' + retval[23:]

    print '--', time.strftime("%H:%M:%s")
    for i in range(0, len(frame), 16):
        line = frame[i:i+16]
        print '%04X  %-49s |%-17s|' % (i, to_hex(line), to_chr(line))


ETH_P_ALL = 3

s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(ETH_P_ALL))
while 1:
    hexdump(s.recv(1600))

