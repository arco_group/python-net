#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import sys
import socket
import struct


def cksum(data):

    def sum16(data):
        "sum all the 16-bit words in data"
        if len(data) % 2:
            data += '\0'

        return sum(struct.unpack("!%sH" % (len(data) // 2), data))

    retval = sum16(data)                       # sum
    retval = sum16(struct.pack('!L', retval))  # one's complement sum
    retval = (retval & 0xFFFF) ^ 0xFFFF        # one's complement
    return retval


def syn_segment():
    dst_port = 80
    src_port = 12345
    seq = 1
    ack = 0
    head_len = 0x50
    flags = 0x02
    window = 8192
    urg_pointer = 0
    payload = 'payload'

    def pack(checksum=0):
        tcp_header0 = struct.pack('!2HII', src_port, dst_port, seq, ack) + \
                      struct.pack('!2bH', head_len, flags, window)
        tcp_header1 = struct.pack('!2H', checksum, urg_pointer)
        return tcp_header0 + tcp_header1 + payload.encode()

    segment = pack()
    return pack(cksum(segment))


host = sys.argv[1]

sock = socket.socket(socket.AF_INET, socket.SOCK_RAW,
                     socket.getprotobyname('tcp'))

sock.sendto(syn_segment(), (host, 0))
