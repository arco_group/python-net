#!/usr/bin/python3
# -*- coding:utf-8; mode:python -*-

import sys
from scapy.all import IP, TCP, RandShort, srloop

MANY = 10

host = sys.argv[1]
pkt = IP(dst=host) / TCP(dport=80, sport=RandShort(), flags='S') / 'payload'
ok, fail = srloop(pkt, inter=0.01, timeout=5, count=MANY)
ok.summary()
