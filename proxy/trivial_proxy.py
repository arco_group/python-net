#!/usr/bin/python
"Usage: %s <dest_ip> <dest_port> <local_port>"

from sys import argv
from socket import *
import os, select

def proxy_handler(target, sock):
   
   remote_sock = socket(AF_INET, SOCK_STREAM)
   remote_sock.connect(target)
		
   while 1:
      rd = select.select([sock, remote_sock],[],[])[0]
      if sock in rd:
         msg = sock.recv(1024)
         if not msg: break
         remote_sock.sendall(msg)
         
      if remote_sock in rd:
         msg = remote_sock.recv(1024)
         if not msg: break
         sock.sendall(msg)

   remote_sock.close()
   

if len(argv) != 4:
   print __doc__ % argv[0]
   exit(1)

destination = (argv[1], int(argv[2]))
ssock = socket(AF_INET, SOCK_STREAM)
ssock.bind(('', int(argv[3])))
ssock.listen(1)

while 1:
   child_sock, addr = ssock.accept()
   proxy_handler(destination, child_sock)
