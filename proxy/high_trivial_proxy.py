#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
"Usage: %s <dest_ip> <dest_port> [local_port]"

from sys import argv
import os, select, socket, SocketServer


class Proxy(SocketServer.StreamRequestHandler):
	def handle(self):
		print 'Client connected:', self.client_address
		dest = socket.socket()
		dest.connect(destination)
		
		while 1:
			leg = select.select([self.rfile, dest],[],[])[0]
			if self.rfile in leg:
				msg = os.read(self.rfile.fileno(), 1024)
				if not msg: break
				dest.sendall(msg)
				
			if dest in leg:
				msg = dest.recv(1024)
				if not msg: break
				self.wfile.write(msg)
				self.wfile.flush()

		dest.close()
		print 'Client disconnected:', self.client_address


if len(argv) not in [3,4]:
   print __doc__ % argv[0]
else:
	destination  = (argv[1], int(argv[2]))
	if len(argv) == 4: local_port = int(argv[3])
	else: local_port = destination[1]
	SocketServer.ForkingTCPServer(('', local_port), Proxy).serve_forever()
	
